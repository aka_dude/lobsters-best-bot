use std::fmt;

use chrono::{DateTime, FixedOffset};
use serde::de::{Deserializer, Error, Visitor};

struct DateTimeVisitor;

impl<'de> Visitor<'de> for DateTimeVisitor {
    type Value = DateTime<FixedOffset>;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "an ISO 8601 date and time string")
    }

    fn visit_str<E>(self, s: &str) -> Result<Self::Value, E>
    where
        E: Error,
    {
        DateTime::parse_from_rfc3339(s).map_err(E::custom)
    }
}

pub(crate) fn deser_datetime<'de, D>(deser: D) -> Result<DateTime<FixedOffset>, D::Error>
where
    D: Deserializer<'de>,
{
    deser.deserialize_str(DateTimeVisitor)
}
