use scraper::{ElementRef, Html, Node};

#[derive(Debug)]
pub enum CaptionError {
    InvalidHtml,
    UnprocessableBaseTag,
    UnprocessableNode,
    UnsupportedBullet,
}

const TAB: &str = "  ";

pub fn escape_text(text: &str, offset: &str) -> Option<String> {
    if !text.trim_matches('\n').is_empty() {
        Some(
            text.replace("&", "&amp;")
                .replace("<", "&lt;")
                .replace(">", "&gt;")
                .replace("\n", &["\n", offset].concat()),
        )
    } else {
        None
    }
}

fn inner_to_text(elem: ElementRef<'_>, offset: &str) -> Result<String, CaptionError> {
    elem.children()
        .filter_map(|child| match child.value() {
            Node::Element(_) => Some(element_to_text(ElementRef::wrap(child).unwrap(), offset)),
            Node::Text(text) => escape_text(text, offset).map(Ok),
            Node::Comment(_) => None,
            other => {
                log::error!("Unprocessable node: {:?}", other);
                Some(Err(CaptionError::UnprocessableNode))
            }
        })
        .collect()
}

fn element_to_text(elem: ElementRef<'_>, offset: &str) -> Result<String, CaptionError> {
    Ok(match elem.value().name() {
        "p" => inner_to_text(elem, offset)?,
        "strong" => ["<b>", &inner_to_text(elem, offset)?, "</b>"].concat(),
        "em" => ["<i>", &inner_to_text(elem, offset)?, "</i>"].concat(),
        "code" => ["<code>", &inner_to_text(elem, offset)?, "</code>"].concat(),
        "br" | "hr" => "\n".to_string(),
        "a" => {
            let url = match elem.value().attr("href") {
                Some(url) => url,
                None => {
                    log::warn!("a-tag without href");
                    ""
                }
            };
            let inner = inner_to_text(elem, offset)?;
            ["<a href=\"", url, "\">", &inner, "</a>"].concat()
        }
        "blockquote" => blockquote_to_text(elem, offset)?,
        "ul" => list_to_text(elem, &[offset, TAB].concat())?,
        name => {
            log::warn!("Unprocessable element: {}", name);
            let inner = inner_to_text(elem, offset)?;
            ["&lt;", name, "&gt;", &inner, "&lt;/", name, "&gt;"].concat()
        }
    })
}

fn list_to_text(elem: ElementRef<'_>, offset: &str) -> Result<String, CaptionError> {
    let mut pieces = elem
        .children()
        .filter_map(|child| {
            Some(match child.value() {
                Node::Element(elem) if elem.name() == "li" => Ok([
                    offset,
                    "· ",
                    &match inner_to_text(ElementRef::wrap(child).unwrap(), &[offset, TAB].concat())
                    {
                        Ok(text) => text,
                        Err(err) => return Some(Err(err)),
                    },
                ]
                .concat()),
                Node::Text(text) => return escape_text(text, offset).map(Ok),
                other => {
                    log::error!("Unsupported element in bullet list: {:?}", other);
                    Err(CaptionError::UnsupportedBullet)
                }
            })
        })
        .fuse();
    let mut res = match pieces.next() {
        Some(piece) => piece?,
        None => return Ok(String::new()),
    };
    for piece in pieces {
        res.push('\n');
        res.push_str(TAB);
        res.push_str(&piece?);
    }
    Ok(res)
}

fn blockquote_to_text(elem: ElementRef<'_>, offset: &str) -> Result<String, CaptionError> {
    Ok([
        "&lt;&lt;\n",
        TAB,
        &inner_to_text(elem, &[offset, TAB].concat())?,
        "\n",
        TAB,
        "&gt;&gt;",
    ]
    .concat())
}

pub fn caption_to_text(caption: &str) -> Result<String, CaptionError> {
    let html = Html::parse_fragment(caption);
    if !html.errors.is_empty() {
        return Err(CaptionError::InvalidHtml);
    }
    let mut pieces = html
        .root_element()
        .children()
        .filter_map(|child| {
            match child.value() {
                Node::Element(_) => (),
                Node::Text(text) => return escape_text(text, "").map(Ok),
                other => {
                    // TODO: don't log errors here
                    log::error!("unknown node: {:?}", other);
                    return Some(Err(CaptionError::UnprocessableBaseTag));
                }
            }
            Some(element_to_text(ElementRef::wrap(child).unwrap(), ""))
        })
        .fuse();
    let mut res = match pieces.next() {
        Some(piece) => piece?,
        None => return Ok(String::new()),
    };
    for piece in pieces {
        res.push('\n');
        res.push_str(TAB);
        res.push_str(&piece?);
    }
    Ok(res)
}
