use anyhow::{Context, Result};
use chrono::{DateTime, FixedOffset};
use itertools::Itertools;
use reqwest::Client;
use serde::Deserialize;

const DOMAIN: &str = "https://lobste.rs/";

/// lobste.rs client
#[derive(Default)]
pub struct LClient {
    client: Client,
}

impl LClient {
    pub async fn front_page(&self) -> Result<Vec<Post>> {
        self.client
            .get(DOMAIN)
            .header("accept", "application/json")
            .send()
            .await
            .context("Getting front page")?
            .json()
            .await
            .context("Parsing JSON from server response")
    }
}

#[derive(Debug, Deserialize)]
pub struct Post {
    // pub short_id: String,
    // pub short_id_url: String, // url
    pub created_at: DateTime<FixedOffset>,
    pub title: String,
    pub url: String, // url
    pub score: u32,
    // pub upvotes: u32,
    // pub downvotes: u32,
    pub comment_count: u32,
    pub description: String,
    pub comments_url: String, // url
    pub submitter_user: User,
    pub tags: Vec<String>,
}

macro_rules! ct_dct {
    ($($key:pat => $val:expr),*) => {
        |val| match val {
            $($key => $val),*
        }
    }
}

impl Post {
    pub fn to_msg(&self) -> String {
        let translate_hashtag = ct_dct! {
            "c++" => "cpp".to_string(),
            other => other.replace('-', "_")
        };

        format!(
            concat!(
                "<b>{title}</b> ",
                "by <a href=\"{author_url}\">{author_name}</a>\n",
                "<a href=\"{url}\">[source]</a> <a href=\"{comments_url}\">[comments]</a>",
                "{hashtags}",
                "{description}"
            ),
            title = &crate::caption_to_text::escape_text(&self.title, "").unwrap_or_default(),
            author_url = &self.submitter_user.profile_url(),
            author_name = &self.submitter_user.username,
            url = &self.url,
            comments_url = &self.comments_url,
            hashtags = if !self.tags.is_empty() {
                format!(
                    "\n{}",
                    &self
                        .tags
                        .iter()
                        .map(|tag| format!("#{}", translate_hashtag(tag)))
                        .join(" ")
                )
            } else {
                String::new()
            },
            description =
                if !self.description.trim().is_empty() && self.description.trim().len() <= 4096 {
                    match crate::caption_to_text::caption_to_text(self.description.trim()) {
                        Ok(text) => format!("\n{}", text),
                        Err(err) => {
                            log::error!("Error preparing caption: {:?}", err);
                            return String::new();
                        }
                    }
                } else {
                    String::new()
                }
        )
    }
}

#[derive(Debug, Deserialize)]
pub struct User {
    pub username: String,
    // pub created_at: String, // timestamp
    // pub is_admin: bool,
    // pub about: String,
    // pub is_moderator: bool,
    // pub karma: u32,
    // pub avatar_url: String, // url without domain
    // pub invited_by_user: String,
    // pub github_username: String
}

impl User {
    pub fn profile_url(&self) -> String {
        const PATH: &str = "u/";
        [DOMAIN, PATH, &self.username].concat()
    }
}
