#[derive(Clone, Copy, serde::Deserialize)]
pub struct Config<'a> {
    pub channel_id: i64,
    pub freq: u32,
    pub token: &'a str,
    pub init_datetime_file: &'a str,
}
