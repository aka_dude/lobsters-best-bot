#![feature(try_blocks)]

use std::{io::ErrorKind, time::Duration};

use anyhow::Context;
use chrono::{DateTime, Local};
use futures::{
    future::abortable,
    stream::{self, TryStreamExt},
};
use itertools::Itertools;
use teloxide_core::{
    adaptors::throttle::Limits,
    // bot::{Bot, Limits},
    payloads::SendMessageSetters,
    requests::{Request, Requester, RequesterExt},
    types::{Message, ParseMode},
    // BotBuilder,
    Bot,
};
use tokio::fs;
use tokio_stream::StreamExt;

mod api;
mod caption_to_text;
mod config;

use config::Config;

async fn mainloop(bot: Bot, cli: &api::LClient, config: config::Config<'_>) -> anyhow::Error {
    let bot = &bot.throttle(Limits {
        messages_per_sec_chat: 1,
        messages_per_sec_overall: 1,
        messages_per_min_chat: 60,
    });

    let me = match bot
        .get_me()
        .send()
        .await
        .context("Request GetMe from server")
    {
        Ok(me) => me,
        Err(err) => return err,
    };
    log::info!("Connection to Telegram API successful");
    log::debug!("Me: {:#?}", me);

    let last_post_created_at = match fs::read_to_string(config.init_datetime_file).await
    {
        Ok(datetime) => {
            match DateTime::parse_from_rfc3339(datetime.trim()).context("Parse initial datetime") {
                Ok(datetime) => datetime,
                Err(err) => return err.context(r#"Read "init_datetime" file contents"#),
            }
        }
        Err(err) if err.kind() == ErrorKind::NotFound => Local::now().into(),
        Err(err) => return anyhow::Error::new(err).context(r#"Read "init_datetime" file contents"#),
    };

    let looped = stream::unfold(
        last_post_created_at,
        |mut last_post_created_at| async move {
            let res: anyhow::Result<()> = try {
                log::debug!("New cycle starts");
                let new_posts = match cli.front_page().await {
                    Ok(val) => val,
                    Err(err) => {
                        log::error!("{:?}", err);
                        return Some((Ok(()), last_post_created_at));
                    }
                };
                for post in new_posts
                    .iter()
                    .filter(|post| post.created_at > last_post_created_at)
                    .sorted_by(|a, b| a.created_at.cmp(&b.created_at))
                {
                    let message = api::Post::to_msg(post);
                    if message.is_empty() {
                        log::warn!("Post generates empty message: {:?}", post);
                        continue;
                    }
                    log::debug!("=== NEW POST ===\n{}\n=== END ===", message);
                    let _: Message = bot
                        .send_message(config.channel_id, message)
                        .parse_mode(ParseMode::Html)
                        .disable_web_page_preview(false)
                        .send()
                        .await
                        .context("Send message to channel")?;
                    last_post_created_at = post.created_at;
                }
                fs::write(config.init_datetime_file, last_post_created_at.to_rfc3339())
                    .await
                    .context(r#"Dump "init_datetime" to file"#)?;
            };
            Some((
                res.map_err(|err| (err, last_post_created_at)),
                last_post_created_at,
            ))
        },
    );

    let (err, last_post_created_at) = looped
        .throttle(Duration::from_secs(config.freq as u64))
        .try_collect::<()>()
        .await
        .unwrap_err();
    match fs::write(config.init_datetime_file, last_post_created_at.to_rfc3339())
        .await
        .context(r#"Dump "init_datetime" to file"#)
    {
        Ok(_) => err,
        Err(err2) => err2.context(err),
    }
}

fn main() -> anyhow::Result<()> {
    env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("info")).init();

    let rt = tokio::runtime::Builder::new_current_thread()
        .enable_all()
        .build()
        .expect("Create tokio runtime");

    let cli = api::LClient::default();

    let config_path = std::env::args().skip(1).next().unwrap_or_else(|| "config.toml".to_string());

    let config_raw = std::fs::read_to_string(config_path).expect("Open config file");
    let config: Config = toml::from_str(&config_raw).expect("Parse config from file");

    let bot = Bot::new(config.token);

    let (mainloop_fut, mainloop_handle) = abortable(mainloop(bot, &cli, config));
    ctrlc::set_handler(move || mainloop_handle.abort()).expect("Error setting Ctrl-C handler");

    if let Ok(err) = rt.block_on(mainloop_fut) {
        log::error!("{:?}", err);
        return Err(err);
    }

    Ok(())
}
