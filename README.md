`config.toml` must contain:
- `channel_id = ` where to post
- `freq = ` how many seconds to wait between checking lobste.rs
- `init_datetime_file = ` path to file where time of creation of last sent post is preserved

Here is running instance: https://t.me/lobstersfrontpage