FROM rust:1 AS build

WORKDIR /root/build

# RUN cargo init --vcs none --name dummy

RUN rustup default nightly

COPY Cargo.toml .
COPY Cargo.lock .

# RUN cargo build --release
#  && rm -r src

COPY src src

RUN cargo build --release

FROM gcr.io/distroless/cc:sha256-6c74d8a13b2c948320440285ff0ffd6ca78287913e6801ab88d23fbe6af51731

WORKDIR /root/

COPY --from=build /root/build/target/release/lobsters-best-bot .

VOLUME ["/root/data/"]
VOLUME ["/root/config/"]

ENTRYPOINT ["/root/lobsters-best-bot", "/root/config/config.toml"]
